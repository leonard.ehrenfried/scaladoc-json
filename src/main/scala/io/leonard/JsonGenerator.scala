package io.leonard

import java.io.PrintWriter

import play.api.libs.json.Json

import scala.tools.nsc.doc.doclet.{ Generator, Indexer, Universer }
import scala.tools.nsc.doc.model._

case class Type(qualifiedName: String, kind: String, members: Seq[String])

class JsonGenerator extends Generator with Universer with Indexer {

  override def generateImpl() {
    printPackage(universe.rootPackage)
  }

  def printPackage(p: Package): Unit = {
    writeTemplate(p.templates)
    p.packages.foreach(printPackage)
  }

  def writeTemplate(templates: List[TemplateEntity with MemberEntity]) = {
    templates.foreach { t ⇒
      val converted = convert(t)
      val filename = s"output/${converted.qualifiedName}.json"

      println(s"Writing to $filename")
      new PrintWriter(filename) { write(Json.toJson(converted).toString()); close() }
    }
  }

  def convert(t: TemplateEntity with MemberEntity): Type = {
    val members = t match {
      case docTemplate: DocTemplateEntity ⇒ getMembers(docTemplate)
      case v: Def                         ⇒ Seq(v.name)
      case _                              ⇒ Seq.empty
    }
    Type(t.qualifiedName, t.kind, members)
  }

  def getMembers(entity: DocTemplateEntity): Seq[String] =
    entity.members.map(_.name)

  implicit val typeFormat = Json.format[Type]
}