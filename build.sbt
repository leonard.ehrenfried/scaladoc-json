import scala.xml.NodeSeq
import scalariform.formatter.preferences._

val commonSettings = Seq(
  scalaVersion := "2.11.7",
  organization := "io.leonard",
  scalacOptions ++= Seq("-Xfatal-warnings", "-feature"))

lazy val `scaladoc-json` = project.in(file(".")).
  configs(IntegrationTest).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Seq(
      "com.typesafe.play" %% "play-json"      % "2.4.6",
      "org.scalatest"     %% "scalatest"      % "2.1.3" % "it,test",
      "org.scala-lang"    %  "scala-compiler" % "2.11.7"
    )
  )

scalariformSettings ++ Seq(
  ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference(AlignSingleLineCaseStatements, true)
    .setPreference(DoubleIndentClassDeclaration, true)
    .setPreference(PreserveDanglingCloseParenthesis, true)
    .setPreference(PreserveSpaceBeforeArguments, true)
    .setPreference(RewriteArrowSymbols, true)
)

