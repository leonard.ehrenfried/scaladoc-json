classpath = target/scala-2.11/classes:target/scala-2.11/scaladoc-json-assembly-0.1-SNAPSHOT.jar
scala_version = 2.11.7
filename = v${scala_version}.tar.gz

default: download compile scaladoc

compile:
	sbt assembly

scaladoc:
	scaladoc -toolcp ${classpath} \
		-doc-generator io.leonard.JsonGenerator \
		download/scala-2.11.7/src/library/scala/**/*.scala
	mkdir -p output


download:
	mkdir download
	wget https://github.com/scala/scala/archive/${filename} -P download
	tar -zxvf download/${filename} -C download

clean:
	rm -rf download
